CC = gcc
CFLAGS = -c -Wall

TARGET = main_image
SOURCES = $(wildcard *.c)
OBJECTS = $(patsubst %.c, %.o, $(SOURCES))

build: $(TARGET)
	echo Built!

clear:
	rm -rf $(TARGET) *.o
	echo Cleared!
	
$(TARGET) : $(OBJECTS)
	$(CC) -o $@ $^

%.o : %.c
	$(CC) $(CFLAGS) -o $@ $^
